#include "opengl-application.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

#include <iostream>
#include <memory>

static const char* vertex_str = "#version 330 core\n"
	"layout(location = 0) in vec3 in_Position;"
	"layout(location = 1) in vec3 in_Color;"
	"out vec3 out_VertColor;"
	"uniform mat4 MVP;"
	"void main() {"
		"gl_Position = MVP * vec4(in_Position, 1.0);"
		"out_VertColor = in_Color;"
	"}"
;

static const char* fragment_str = "#version 330 core\n"
	"precision highp float;"
	"in vec3 out_VertColor;"
	"out vec4 out_FragColor;"
	"void main() {"
		"out_FragColor = vec4(out_VertColor, 1.0);"
	"}"
;

OpenGLApplication::OpenGLApplication()
	: SDLApplication(), m_context(), m_distance(1.0f), m_xangle(0), m_yangle(0), m_vertex_array(0), m_program(0), m_mvp(0)
{
}

void OpenGLApplication::draw(std::shared_ptr<Shape> shape) const
{
	static const GLfloat data[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f,  1.0f, 0.0f,
	};

	static const GLfloat color[] = {
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
	};

	static const int VBO_MAX = 2;
	GLuint vertex_buffer[VBO_MAX];

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glGenBuffers(VBO_MAX, vertex_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[0]);

	for (const Point& point: shape->get_points()) {
		const glm::vec3 vertex = point.get_vertex();

		glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), glm::value_ptr(vertex), GL_STATIC_DRAW);
	}

	// Set points in the VBO
//	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[0]);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

	// Sets points in the VAO
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[0]);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Set colors in the VBO
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);

	// Sets colors in the VAO
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[1]);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// END
	glUseProgram(m_program);

	glm::mat4 view_matrix = get_view_matrix();
	glUniformMatrix4fv(m_mvp, 1, GL_FALSE, &(view_matrix[0][0]));

	glEnable(GL_CULL_FACE);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	//glDrawArrays(GL_POINTS, 0, 3);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

Application& OpenGLApplication::get()
{
	static std::unique_ptr<Application> singleton = std::make_unique<OpenGLApplication>(OpenGLApplication());

	return *singleton;
}

void OpenGLApplication::build_render()
{
	m_context = SDL_GL_CreateContext(get_window());

	if (glewInit() != 0) {
		throw std::runtime_error("Failed to initialize GLEW");
	}

	glGenVertexArrays(1, &m_vertex_array);
	glBindVertexArray(m_vertex_array);

	init_shaders();

	m_mvp = glGetUniformLocation(m_program, "MVP");
}

void OpenGLApplication::destroy_render()
{
	SDL_GL_DeleteContext(m_context);
}

glm::mat4 OpenGLApplication::get_view_matrix() const
{
	glm::mat4 view_matrix(1.0f);

	glm::mat4 translation = glm::translate(view_matrix, glm::vec3(0.0f, 0.0f, -m_distance));
	glm::mat4 rotate_x = glm::rotate(m_xangle, glm::vec3(1.0f, 0.0f, 0.0f));
	glm::mat4 rotate_y = glm::rotate(m_yangle, glm::vec3(0.0f, 1.0f, 0.0f));

	view_matrix = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f) * translation * rotate_x * rotate_y;

	return view_matrix;
}

void OpenGLApplication::init_shaders()
{
	int length;
	GLint status;
	GLuint vertex_shader;
	GLuint fragment_shader;

	// Vertex Shader
	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_str, nullptr);
	glCompileShader(vertex_shader);

	status = GL_FALSE;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status);
	glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &length);

	if (status == GL_FALSE) {
		GLchar* message = static_cast<GLchar*>(malloc(length + 1));

		glGetShaderInfoLog(vertex_shader, length, nullptr, message);

		throw std::runtime_error(message);
	}

	std::cout << "Vertex shader: DONE" << std::endl;

	// Fragment Shader
	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_str, nullptr);
	glCompileShader(fragment_shader);

	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &status);
	glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &length);

	if (status == GL_FALSE) {
		GLchar* message = static_cast<GLchar*>(malloc(length + 1));

		glGetShaderInfoLog(fragment_shader, length, nullptr, message);

		throw std::runtime_error(message);
	}

	std::cout << "Fragment shader: DONE" << std::endl;

	// Shader Program
	m_program = glCreateProgram();

	glAttachShader(m_program, vertex_shader);
	glAttachShader(m_program, fragment_shader);

	glBindAttribLocation(m_program, 0, "in_Position");
	glBindAttribLocation(m_program, 1, "in_Color");

	glLinkProgram(m_program);

	glGetProgramiv(m_program, GL_LINK_STATUS, &status);

	if (!status) {
		throw std::runtime_error("Failed to link the program");
	}

	glDetachShader(m_program, vertex_shader);
	glDetachShader(m_program, fragment_shader);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}

void OpenGLApplication::rotate_left_camera(float degrees)
{
	m_xangle += degrees;
	std::cout << "xangle=" << m_xangle << std::endl;
}

void OpenGLApplication::rotate_up_camera(float degrees)
{
	m_yangle += degrees;
	std::cout << "yangle=" << m_yangle << std::endl;
}

void OpenGLApplication::zoom_camera(float degrees)
{
	m_distance += degrees;
	std::cout << "distance=" << m_distance << std::endl;
}
