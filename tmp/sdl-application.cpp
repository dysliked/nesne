#include "sdl-application.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <iostream>

static const float STEP = 0.1f;

SDLApplication::SDLApplication()
	: m_window(nullptr)
{
}

SDL_Window* SDLApplication::get_window() const
{
	return m_window;
}

void SDLApplication::handle_keyboard(SDL_KeyboardEvent* event)
{
	switch (event->keysym.sym) {
		case SDLK_LEFT:
			rotate_up_camera(STEP);
			break;
		case SDLK_RIGHT:
			rotate_up_camera(-STEP);
			break;
		case SDLK_UP:
			rotate_left_camera(STEP);
			break;
		case SDLK_DOWN:
			rotate_left_camera(-STEP);
			break;
		default:
			break;
	}
}

int SDLApplication::run(std::shared_ptr<Shape> shape)
{
	SDL_Event event;

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		throw std::runtime_error(SDL_GetError());
	}

	m_window = SDL_CreateWindow(APPLICATION_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 768, 512, SDL_WINDOW_OPENGL);

	build_render();

	while (SDL_WaitEvent(&event)) {
		if (event.type == SDL_QUIT) {
			break;
		} else if (event.type == SDL_MOUSEWHEEL) {
			zoom_camera(static_cast<float>(event.wheel.y) * STEP);
		} else if (event.type == SDL_KEYUP) {
			handle_keyboard(&(event.key));
		}

		draw(shape);

		SDL_GL_SwapWindow(m_window);
	}

	destroy_render();
	SDL_DestroyWindow(m_window);
	SDL_Quit();

	return 0;
}
