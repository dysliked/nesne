#ifndef __APPLICATION_FACTORY__
#define __APPLICATION_FACTORY__

#include "application.hpp"

class ApplicationFactory
{
public:

	static Application& get(std::string& render_type);
};

#endif // __APPLICATION_FACTORY__
