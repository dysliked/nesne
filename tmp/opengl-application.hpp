#ifndef __OPENGL_APPLICATION__
#define __OPENGL_APPLICATION__

#include "sdl-application.hpp"

#include <GL/glew.h>
#include <GL/gl.h>

class OpenGLApplication : public SDLApplication
{
	friend std::unique_ptr<OpenGLApplication> std::make_unique<OpenGLApplication>();

public:

	virtual ~OpenGLApplication() {}

	void build_render() override;

	void destroy_render() override;

	void draw(std::shared_ptr<Shape> shape) const override;

	static Application& get();

	void rotate_left_camera(float degrees) override;

	void rotate_up_camera(float degrees) override;

	void zoom_camera(float degrees) override;

private:

	explicit OpenGLApplication();

	void init_shaders();

	void init_window();

	glm::mat4 get_view_matrix() const;

	SDL_GLContext m_context;

	float m_distance;
	float m_xangle;
	float m_yangle;

	GLuint m_vertex_array;
	GLuint m_program;
	GLuint m_mvp;
};

#endif // __OPENGL_APPLICATION__
