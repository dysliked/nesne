#ifndef __SDL_APPLICATION__
#define __SDL_APPLICATION__

#include "application.hpp"

#include <SDL2/SDL.h>

class SDLApplication : public Application
{
public:

	virtual ~SDLApplication() {}

	virtual void build_render() = 0;

	virtual void destroy_render() = 0;

	virtual void draw(std::shared_ptr<Shape> shape) const = 0;

	SDL_Window* get_window() const;

	virtual void rotate_left_camera(float degrees) = 0;

	virtual void rotate_up_camera(float degrees) = 0;

	int run(std::shared_ptr<Shape> shape) override;

	virtual void zoom_camera(float delta) = 0;

protected:

	explicit SDLApplication();

	void handle_keyboard(SDL_KeyboardEvent* event);

private:

	SDL_Window* m_window;
};

#endif // __SDL_APPLICATION__
