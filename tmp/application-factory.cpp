#include "application-factory.hpp"

#include "g3x-application.hpp"
#include "opengl-application.hpp"

Application& ApplicationFactory::get(std::string& render_type)
{
	if (render_type == "g3x") {
		return G3XApplication::get();
	} else if (render_type == "opengl") {
		return OpenGLApplication::get();
	} else {
		throw std::runtime_error("unknown application type");
	}
}
