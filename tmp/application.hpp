#ifndef __APPLICATION__
#define __APPLICATION__

#include "shape.hpp"

#include <memory>

class Application
{
public:

	virtual ~Application() {}

	virtual int run(std::shared_ptr<Shape> shape) = 0;
};

#endif /* __APPLICATION__ */
