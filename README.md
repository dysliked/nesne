# Nesne

## Build

```bash
meson setup build
meson compile -C build
```

## Execution

```bash
nesne /path/to/shape.json
```

## Development

### Links

* http://www.opengl-tutorial.org/fr/
* https://www.khronos.org/opengl/wiki/Tutorial2:_VAOs,_VBOs,_Vertex_and_Fragment_Shaders_(C_/_SDL)
