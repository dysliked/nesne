/*=================================================================*/
/*= E.Incerti - incerti@upem.fr                                   =*/
/*= Universit� Paris-Est-Marne-la-Vall�e                          =*/
/*= Exemple de fonctionalit�s de lib. graphique <g3x>             =*/
/*=================================================================*/

#include <g3x.h>

static int N = 1000;
static int P = 500;

static G3Xpoint *pts;
static G3Xvector *normes;

/*= FONCTION DE MODELISATION =*/
static void InitSphere(void)
{
	G3Xpoint *p;
	G3Xvector *n;

	double theta = 2 * PI / N;
	double phi = PI / P;
	
	int i, j;
	double x, y;

	pts = (G3Xpoint*) calloc(N*P, sizeof(G3Xpoint));
	normes = (G3Xvector*) calloc(N*P, sizeof(G3Xvector));

	p = pts;
	n = normes;

	for (i = 0; i < N; i ++)
	{
		x = cos(i * theta);
		y = sin(i * theta);

		for (j = 0; j < P; j ++)
		{
			(*p)[0] = (*n)[0] = x * sin(j*phi);
			(*p)[1] = (*n)[1] = y * sin(j*phi);
			(*p)[2] = (*n)[2] = cos(j*phi);

			p ++;
			n ++;
		}
	}
}

static void InitRandomSphere(void)
{
	G3Xpoint *p;
	G3Xvector *n;

	int i;
	double theta;
	double phi;

	pts = (G3Xpoint*) calloc(N*P, sizeof(G3Xpoint));
	normes = (G3Xvector*) calloc(N*P, sizeof(G3Xvector));

	p = pts;
	n = normes;

	for (i = 0; i < N*P; i ++)
	{
		theta = g3x_Rand_Delta(PI, PI);
		phi = g3x_Rand_Delta(PI / 2, PI / 2);
		
		(*p)[0] = (*n)[0] = cos(theta) * sin(phi);
		(*p)[1] = (*n)[1] = sin(theta) * sin(phi);
		(*p)[2] = (*n)[2] = cos(phi);
		
		p ++;
		n ++;
	}
}

static void InitRandomCube(void)
{
	int id = 0;
	
	G3Xpoint *p;
	G3Xvector *n;
	
	pts = (G3Xpoint*) calloc(N*P, sizeof(G3Xpoint));
	normes = (G3Xvector*) calloc(N*P, sizeof(G3Xvector));

	p = pts;
	n = normes;
	
	while (id < N*P / 3)
	{
		(*p)[0] = g3x_Rand_Delta(0, 1); (*n)[0] = 0;
		(*p)[1] = g3x_Rand_Delta(0, 1); (*n)[1] = 0;
		(*p)[2] = (*n)[2] = ((id % 2 == 0)?-1:1);
		
		p ++; n ++;
		id ++;
	}
	
	while (id < 2*N*P / 3)
	{
		(*p)[0] = g3x_Rand_Delta(0, 1); (*n)[0] = 0;
		(*p)[1] = (*n)[1] = ((id % 2 == 0)?-1:1);
		(*p)[2] = g3x_Rand_Delta(0, 1); (*n)[2] = 0;
		
		p ++; n ++;
		id ++;
	}
	
	while (id < N*P)
	{
		(*p)[0] = (*n)[0] = ((id % 2 == 0)?-1:1);
		(*p)[1] = g3x_Rand_Delta(0, 1); (*n)[1] = 0;
		(*p)[2] = g3x_Rand_Delta(0, 1); (*n)[2] = 0;
		
		p ++; n ++;
		id ++;
	}
}

/*= FONCTION D'ANIMATION =*/
static void Anim(void)
{
	fprintf(stderr,"Anim-");		
}

/*= FONCTION DE DESSIN PRINCIPALE =*/
static void Dessin(void)
{
	G3Xpoint *p;
	G3Xvector *n;
	
	g3x_Material(G3Xr, 0, 1, 0.5, 1, 0.5);
	glBegin(GL_POINTS);

	p = pts;
	n = normes;

	while (p < pts + N*P)
	{
		glNormal3dv(*n); n ++;
		glVertex3dv(*p); p ++;
	}

	glEnd();
}

static void DessinStart(void)
{
	
	glPushMatrix();
	glScalef(2., 2, 0.5); /* Homothetie */
	g3x_Material(G3Xr, 0, 1, 0.5, 1, 0.5);
	glutSolidSphere(1., 40, 40);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1, 1, 1); /* Translation */
	g3x_Material(G3Xb, 0, 1, 0.5, 1, 0.5);
	glutSolidCube(2.);
	glPopMatrix();
}

static void DessinTable(void)
{
	glPushMatrix();
	glScalef(1.5, 1, 0.05);
	g3x_Material(G3Xr, 0, 1, 0.5, 1, 0.5);
	glutSolidCube(2.);
	glPopMatrix();

	glPushMatrix();
	glScalef(0.05, 0.05, 0.5);

	glPushMatrix();
	glTranslatef(25, 15, -1);
	g3x_Material(G3Xg, 0, 1, 0.5, 1, 0.5);
	glutSolidCube(2.);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(25, -15, -1);
	g3x_Material(G3Xg, 0, 1, 0.5, 1, 0.5);
	glutSolidCube(2.);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-25, 15, -1);
	g3x_Material(G3Xg, 0, 1, 0.5, 1, 0.5);
	glutSolidCube(2.);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-25, -15, -1);
	g3x_Material(G3Xg, 0, 1, 0.5, 1, 0.5);
	glutSolidCube(2.);
	glPopMatrix();

	glPopMatrix();

	glPushMatrix();
	glScalef(0.25, 0.25, 0.25);
	glTranslatef(1, 2, 1.075);
	g3x_Material(G3Xy, 0, 1, 0.5, 1, 0.5);
	glutSolidSphere(1., 40, 40);
	glPopMatrix();
}

/*=    ACTION A EXECUTER EN SORTIE   =*/
/*= lib�ration de m�moire, nettoyage =*/
/*= -> utilise la pile de <atexit()> =*/
static void Exit(void)
{
	free(pts);
	free(normes);
  /* rien � faire ici puisqu'il n'y a pas d'allocation dynamique */
	fprintf(stderr,"\nbye !\n");
}


int main(int argc, char** argv)
{ 
  
  /* initialisation de la fen�tre graphique et param�trage Gl */
  g3x_InitWindow(*argv,768,512);

  /* d�finition des fonctions */
  g3x_SetInitFunction(InitRandomSphere);
  g3x_SetExitFunction(Exit  );     /* la fonction de sortie */
  g3x_SetDrawFunction(Dessin);     /* la fonction de Dessin */
	g3x_SetAnimFunction(Anim);
	
	/* boucle d'ex�cution principale */
  return g3x_MainStart();
  /* rien apr�s �a */
}

/*
 * Couleur: typedef float G3Xcolor[4]; (r, g, b, alpha) E [0, 1]^4
 * glColor... pour changer la couleur courante (Nb de composants, le type (f -> float)[, donn�es vectoriels])
 * 
 * g3x_Material(couleur, comment l'objet reflete la lumi�re (constant < 0.25), diffraction (0.75), sp�cularit� (0..1), shine en relation avec le pr�c�dent (proche de 1), transparence toujours � 0)
 * 
 */
