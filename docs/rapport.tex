\documentclass{article}

\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{hyperref}

\title{Rapport du projet d'Infograpie:\\ Rendu par point d'arbre CSG}
\author{Mustapha \textsc{El Mokhtari} \and Thibault \textsc{Peypelut}}
\date{}

\begin{document}

\maketitle
\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

\paragraph{}
Dans le cadre d'un projet de Synthèse d'image en première année de Master informatique, à l'Université Paris-Est Marne-la-Vallée, nous avons dû réaliser un programme faisant aparaître une figure à l'aide de points.

\paragraph{}
Le but du projet est d'utiliser une librairie de programmation graphique 3D.
Pour cela, nous avons choisi d'utiliser les standards de l'université, c'est-à-dire les librairies \verb|OpenGL| et \verb|g3x|.
Ce programme est lancé à partir d'un fichier et ensuite, il crée les formes écrites avec les différentes transformations.

\paragraph{}
Dans un premier temps, nous expliquerons l'architecture du projet, c'est-à-dire les différents modules implémentés.
Ensuite, nous détaillerons les éléments indispensables au bon déroulement du programme.

\newpage

\tableofcontents
\newpage

\section{Architecture}

\paragraph{}
Pour des raisons pédagogiques, nous avons choisi de faire le projet en utilisant le paradigme objet.
De ce fait, nous avons utilisé le langage \verb|C++|.

\subsection{Les modules primaires}

\subsubsection{Module Coord}

\paragraph{}
Ce module permet de représenter les coordonnées dans l'espace.
Avec cet objet, on pourra l'utiliser comme un sommet ou une normale d'un point.
Chaque dimension est un réel.

\label{opp_inv}
\paragraph{}
Sur chaque coordonnée, on pourra obtenir son opposé ou son inverse grâce aux méthodes respective \verb|opposite| et \verb|inverse|.
L'opposé (ou l'inverse) d'une coordonnée s'agit de l'opposé (ou l'inverse) de chaque dimension.

Par exemple, pour la coordonnée $(2, 2, 2)$, son opposé est $(-2, -2, -2)$ et son inverse est $(\frac{1}{2}, \frac{1}{2}, \frac{1}{2})$.

\subsubsection{Module Color}

\paragraph{}
Ce module représente une couleur dans le codage RGB.
Chaque composante est stockée comme un entier naturel, allant de 0 à 255.
Une couleur est égale à une autre si et seulement si chaque composante respective sont égale entre elles.

\paragraph{}
Afin de s'adapter à la librairie \verb|OpenGL|, on a créé plusieurs méthodes permettant d'avoir les composantes en réel (variant de 0 à 1).

\subsubsection{Module Point}

\paragraph{}
Ce module représente un point dans l'espace.
Chaque point est composé d'un sommet (sa position dans l'espace), une normale (le vecteur perpendiculaire au point) et une couleur.

\paragraph{}
Un point peut devenir «invisible» seulement si il se situe à l'intérieur d'une forme (voir la section \ref{inside}).

\subsubsection{Module Transformation}

\paragraph{}
Avec ce module, on pourra faire une translation, une rotation ou une homothétie d'un point.
Pour construire cet objet, on prendra trois paramètres de type \verb|Coord| qui représenteront les trois transformations possibles.
Pour faciliter les calculs entre les vecteurs et les points, on utilise les coordonnées homogènes sous forme matricielle.

\paragraph{}
Chaque transformation est constitué de trois matrices:
\begin{itemize}
	\item la première permet d'appliquer la translation, la rotation ou l'homothétie voulu à un sommet.
	\item la deuxième permet d'annuler la transformation sur un sommet (c'est la matrice inverse de la première). On la calcule en utilisant l'opposé de la translation et de la rotation et l'inverse de l'homothétie (voir la section \ref{opp_inv}).
	\item la troisième permet d'appliquer la transformation à une normale.
\end{itemize}

\paragraph{}
La méthode \verb|transform| consiste à pendre un point en paramètre et de retourner un autre point transformé.
Elle applique la première matrice au sommet et la troisième à la normale du point.

La méthode \verb|cancel| prend les coordonnées du sommet d'un point et retourne les coordonnées du sommet sans la transformation.
Elle applique simplement la deuxième matrice.
Pour cette méthode, connaître la normale du point ne nous est pas util, car nous voulons juste le sommet du point sans la transformation.

La méthode statique \verb|identity| retourne une transformation qui ne change aucun point.
C'est-à-dire qu'on applique la translation $(0, 0, 0)$, la rotation $(0, 0, 0)$ et l'homothétie $(1, 1, 1)$.

\subsection{Module Shape}

\paragraph{}
Ce module est une classe abstraite permettant de représenter une figure.
Elle est constitué d'un ensemble de point et d'une transformation.

\label{inside}
\paragraph{}
La méthode \verb|is_inside| doit être défini par les sous-classes.

\subsubsection{Module SimpleShape}

\paragraph{}
Cette classe hérite de la classe \verb|Shape| et permet de représenter une forme canonique.
C'est dans ses sous-classes que nous définissons la méthode \verb|is_inside|.
Chaqu'une de ces sous-classes auront des dimensions fixes.
Pour changer leur valeur, il suffira d'appliquer à la figure une transformation.

\paragraph{}
Il y a cinq formes canoniques implémentées dans ce projet:
\begin{itemize}
	\item un cône de rayon 1 et d'hauteur 2 (dans le module \verb|Cone|);
	\item un cube de longueur 2 (dans le module \verb|Cube|);
	\item un cylindre de rayon 1 et de hauteur 2 (dans le module \verb|Cylinder|);
	\item une sphère de rayon 1 (dans le module \verb|Sphere|);
	\item un tore de rayon externe 1 et de rayon interne $0.25$ (dans le module \verb|Torus|).
\end{itemize}

\subsubsection{Module OperatorShape}
Cette classe hérite aussi de la classe \verb|Shape| et permet de faire des opérations boolèennes sur deux formes.
La méthode \verb|is_inside| doit aussi être défini dans ses sous-classes.

\paragraph{}
Dans ce projet, nous aurons quatre opérateurs différents:
\begin{itemize}
	\item l'intersection (dans le module \verb|InterShape|);
	\item la substitution (dans le module \verb|SubstShape|);
	\item l'union (dans le module \verb|UnionShape|);
	\item l'identité (dans le module \verb|IdentityShape|) qui affiche tous les points des deux formes.
\end{itemize}

\subsection{Module Render}

\paragraph{}
Ce module est une interface nous permettant d'utiliser une librairie de programmation graphique et aussi de tirer des nombres aléatoirement.

\subsubsection{Module G3XRender}

\paragraph{}
Cette classe implémente l'interface \verb|Render| et permet d'utiliser les librairies \verb|OpenGL| et \verb|g3x|.

\paragraph{}
La méthode \verb|random| doit absolument prendre un intervalle positif.

\subsection{Module Parser}

\paragraph{}
Ce module nous permet de construire une forme à partir d'un fichier texte (format: voir section \ref{format}).

\section{Utilisation du programme}

\subsection{Compilation et exécution}

\paragraph{}
Comme le programme est écrit en \verb|C++|, il faut le compiler à l'aide du \verb|Makefile| en tapant la commande \verb|make|.
Pour avoir le rendu d'une forme, il est impératif de lancer le programme avec un fichier en argument.
Le format du fichier est décrit dans la section \ref{format}.

\paragraph{}
Lors de la création de ce projet, nous avons débuté en utilisant la version \verb|6.3.1| de \verb|g++|.
C'est-à-dire, nous avons utilisé la version \verb|11| du \verb|C++|.

En essayant de l'exécuter à l'université, nous n'y sommes pas parvenu car le compilateur voulait une version moins récente (\verb|C++98|).
Ainsi, nous avons ajouté une option pour le compilateur dans le \verb|Makefile| (\verb|-std=c++98|).
Elle nous permet de compiler les sources dans la version \verb|98| du \verb|C++|.

Depuis qu'on a fait les modifications, nous n'avons pas essayer de le lancer à l'université.

\subsection{Format du fichier}
\label{format}

\paragraph{}
Pour faciliter la lecteur d'un fichier pour une machine, on utilise la notation polonaise inverse.
C'est-à-dire, on met d'abord deux opérandes puis une opération.

\paragraph{}
La première ligne correspond à la forme voulu, cela peut-être une forme canonique (ex: «\verb|sphere|») ou une opération (ex: «\verb|union|»).
Ensuite, sur trois autres lignes, nous y mettons la translation, la rotation et l'homothétie à appliquer à la forme.
Chaqu'une de ces lignes est composée de trois réels correspondant au trois dimensions (x, y, z).

Pour une forme canonique, on doit aussi ajouter sa couleur en RGB sur une nouvelle ligne.
Cette ligne est constitué de trois entiers variant de 0 à 255.

\paragraph{}
\begin{tabular}{ | c | c | }
	\hline
	opérandes, formes canoniques & \verb|cone|, \verb|cube|, \verb|cylinder|, \verb|sphere|, \verb|torus| \\ \hline
	opérations & \verb|identity|, \verb|intersection|, \verb|union|, \verb|substitution| \\ \hline
\end{tabular}



\paragraph{}
Dans le dossier \verb|example|, il y a des exemples de formes que le programme peut exécuter.

\end{document}
