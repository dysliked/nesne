#include "coord-builder.hpp"

CoordBuilder::CoordBuilder()
	: m_x(0), m_y(0), m_z(0)
{
}

glm::vec3 CoordBuilder::build() const
{
	return glm::vec3(m_x, m_y, m_z);
}

double CoordBuilder::get_x() const
{
	return m_x;
}

double CoordBuilder::get_y() const
{
	return m_y;
}

double CoordBuilder::get_z() const
{
	return m_z;
}

void CoordBuilder::set_x(double x)
{
	m_x = x;
}

void CoordBuilder::set_y(double y)
{
	m_y = y;
}

void CoordBuilder::set_z(double z)
{
	m_z = z;
}

std::istream& operator>>(std::istream& is, CoordBuilder& builder)
{
	double xx, yy, zz;

	is >> xx >> yy >> zz;

	builder.set_x(xx);
	builder.set_y(yy);
	builder.set_z(zz);

	return is;
}
