#ifndef __SUBSTRACTION__
#define __SUBSTRACTION__

#include "thing.hpp"

class Substraction : public Shape {

public:

	bool is_inside(const Point& point) const override;

	static std::unique_ptr<Thing> substract(Thing& lhs, Thing& rhs);

private:

	explicit Substraction(std::shared_ptr<Shape> lhs, std::shared_ptr<Shape> rhs)
		: m_lhs(lhs), m_rhs(rhs)
	{}

	std::shared_ptr<Shape> m_lhs;
	std::shared_ptr<Shape> m_rhs;
};

#endif /* __SUBSTRACTION__ */
