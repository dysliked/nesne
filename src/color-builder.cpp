#include "color-builder.hpp"

ColorBuilder::ColorBuilder()
	: m_blue(0), m_green(0), m_red(0)
{
}

glm::vec3 ColorBuilder::build() const
{
	return glm::vec3(m_red / 255.0, m_green / 255.0, m_blue / 255.0);
}

unsigned char ColorBuilder::get_blue() const
{
	return m_blue;
}

unsigned char ColorBuilder::get_green() const
{
	return m_green;
}

unsigned char ColorBuilder::get_red() const
{
	return m_red;
}

void ColorBuilder::set_blue(unsigned char blue)
{
	m_blue = blue;
}

void ColorBuilder::set_green(unsigned char green)
{
	m_green = green;
}

void ColorBuilder::set_red(unsigned char red)
{
	m_red = red;
}

std::istream& operator>>(std::istream& is, ColorBuilder& builder)
{
	int red, green, blue;

	is >> red >> green >> blue;

	builder.set_red(red);
	builder.set_green(green);
	builder.set_blue(blue);

	return is;
}
