#include "g3x-application.hpp"

#include <g3x.h>

static Thing* g_thing;

static void c_anim()
{
}

static void c_draw()
{
	glBegin(GL_POINTS);

	for (const Point& point: g_thing->get_points())
	{
		const glm::vec3& color = point.get_color();
		float rcolor[] = {color[0], color[1], color[2], 1.0};

		g3x_Material(rcolor, 0.25, 0.75, 0.5, 1.0, 0.0);

		const glm::vec3& normal = point.get_normal();
		const glm::vec3& vertex = point.get_vertex();

		glNormal3d(normal[0], normal[1], normal[2]);
		glVertex3d(vertex[0], vertex[1], vertex[2]);
	}

	glEnd();
}

static void c_exit()
{
}

static void c_init()
{
}

G3XApplication::G3XApplication()
	: m_progname(APPLICATION_NAME)
{
}

G3XApplication& G3XApplication::get()
{
	static std::unique_ptr<G3XApplication> singleton = std::make_unique<G3XApplication>(G3XApplication());

	return *singleton;
}

int G3XApplication::run(Thing* thing)
{
	g_thing = thing;

	g3x_InitWindow(const_cast<char*>(m_progname.c_str()), 768, 512);

	g3x_SetInitFunction(c_init);
	g3x_SetExitFunction(c_exit);
	g3x_SetDrawFunction(c_draw);
	g3x_SetAnimFunction(c_anim);

	return g3x_MainStart();
}
