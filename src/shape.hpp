#ifndef __SHAPE__
#define __SHAPE__

#include "point.hpp"

/*
#include <functional>

template<typename T>
class is_shape {
public:
	static const bool value = false;
};

template<typename T>
inline constexpr bool is_shape_v = is_shape<T>::value;

template<typename T>
concept shapable = is_shape_v<T> &&
	requires(T prop, const Point& point) {
		std::invoke(&T::is_inside, std::forward<T>(prop), point);
	};

*/

class Shape {
public:

	virtual bool is_inside(const Point& point) const = 0;
};

#endif /* __SHAPE__ */