#include "intersection.hpp"

#include <algorithm>

std::unique_ptr<Thing> Intersection::intersect(Thing& lhs, Thing& rhs) {
	std::vector<Point> points;
	std::vector<Point> lhs_points = lhs.get_points();
	std::vector<Point> rhs_points = rhs.get_points();

	const auto l_shape = lhs.get_shape();
	const auto r_shape = rhs.get_shape();

	std::copy_if(lhs_points.begin(), lhs_points.end(), std::back_inserter(points), [r_shape](const Point& point){ return r_shape->is_inside(point); });
	std::copy_if(rhs_points.begin(), rhs_points.end(), std::back_inserter(points), [l_shape](const Point& point){ return l_shape->is_inside(point); });

	auto intersection = std::make_shared<Intersection>(Intersection(lhs.get_shape(), rhs.get_shape()));

	return std::make_unique<Thing>(points, intersection);
}

bool Intersection::is_inside(const Point& point) const
{
	return m_lhs->is_inside(point) && m_rhs->is_inside(point);
}

