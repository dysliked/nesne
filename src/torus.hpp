#ifndef __TORUS__
#define __TORUS__

#include "thing.hpp"

class Torus : public Shape {

public:

	static std::unique_ptr<Thing> create(int nb_points, const glm::vec3& color);

	bool is_inside(const Point& point) const override;

private:

	explicit Torus()
	{}
};

#endif /* __TORUS__ */
