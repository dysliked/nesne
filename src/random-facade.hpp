#ifndef __RANDOM_FACADE__
#define __RANDOM_FACADE__

#include <memory>
#include <random>

class RandomFacade
{
	friend std::unique_ptr<RandomFacade> std::make_unique<RandomFacade>();

public:

	virtual ~RandomFacade() {}

	static RandomFacade& get();

	double on_circle();

	double on_line();

private:

	RandomFacade();

	std::default_random_engine m_generator;
	std::uniform_real_distribution<double> m_circle_dist;
	std::uniform_real_distribution<double> m_line_dist;
};

#endif // __RANDOM_FACADE__
