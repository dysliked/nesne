#ifndef __INTERSECTION__
#define __INTERSECTION__

#include "thing.hpp"

class Intersection : public Shape {

public:

	static std::unique_ptr<Thing> intersect(Thing& lhs, Thing& rhs);

	bool is_inside(const Point& point) const override;

private:

	explicit Intersection(std::shared_ptr<Shape> lhs, std::shared_ptr<Shape> rhs)
		: m_lhs(lhs), m_rhs(rhs)
	{}

	std::shared_ptr<Shape> m_lhs;
	std::shared_ptr<Shape> m_rhs;
};

#endif /* __INTERSECTION__ */
