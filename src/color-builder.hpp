#ifndef __COLOR_BUILDER__
#define __COLOR_BUILDER__

#include <glm/glm.hpp>

#include <iostream>

class ColorBuilder
{
public:

	explicit ColorBuilder();

	virtual ~ColorBuilder() {}

	glm::vec3 build() const;

	unsigned char get_blue() const;

	unsigned char get_green() const;

	unsigned char get_red() const;

	void set_blue(unsigned char blue);

	void set_green(unsigned char green);

	void set_red(unsigned char red);

private:

	unsigned char m_blue;
	unsigned char m_green;
	unsigned char m_red;
};

std::istream& operator>>(std::istream& is, ColorBuilder& builder);

#endif // __COLOR_BUILDER__
