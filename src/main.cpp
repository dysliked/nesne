#include "parser.hpp"
#include "g3x-application.hpp"

#include <any>
#include <iostream>
#include <memory>

#include <getopt.h>

int optind;
char* optarg;

void usage(const char* binname)
{
	std::cout
		<< "Usage : " << binname << " [OPTIONS] FILENAME" << std::endl
		<< std::endl
		<< "Options :" << std::endl
		<< "  -?, --help    Print this message" << std::endl
		;

}

int main(int argc, const char* argv[])
{
	static struct option long_options[] = {
		{"help", no_argument, nullptr, '?'},
		{"render", required_argument, nullptr, 'r'},
	};

	int car = 0;
	int opt_index = 0;
	std::string render_type("g3x");

	while ((car = getopt_long(argc, const_cast<char**>(argv), "r:?", long_options, &opt_index)) != -1) {
		switch (car) {
			case '?':
				usage(argv[0]);
				return 0;
			default:
				std::cerr << argv[0] << ": unrecognized option '" << static_cast<char>(car) << "'" << std::endl;
				usage(argv[0]);
				return 1;
		}
	}

	if (optind >= argc) {
		std::cerr << argv[0] << ": argument missing" << std::endl;
		usage(argv[0]);
		return 1;
	}

	G3XApplication& application = G3XApplication::get();
	Parser parser(argv[optind]);

	auto thing = parser.parse();

	return application.run(thing.get());
}
