#include "parser.hpp"

#include "color-builder.hpp"
#include "coord-builder.hpp"

#include "cone.hpp"
#include "cube.hpp"
#include "cylinder.hpp"
#include "hollow.hpp"
#include "sphere.hpp"
#include "torus.hpp"
#include "triangle.hpp"

#include "transformation.hpp"

#include "intersection.hpp"
#include "substraction.hpp"
#include "union.hpp"

#include <boost/property_tree/json_parser.hpp>

static glm::vec3 parse_color(const boost::property_tree::ptree& ptree)
{
	ColorBuilder builder;

	for (const auto& elem: ptree) {
		if (elem.first == "red") {
			builder.set_red(std::stoi(elem.second.data()));
		} else if (elem.first == "green") {
			builder.set_green(std::stoi(elem.second.data()));
		} else if (elem.first == "blue") {
			builder.set_blue(std::stoi(elem.second.data()));
		}
	}

	return builder.build();
}

static glm::vec3 parse_coord(const boost::property_tree::ptree& ptree)
{
	CoordBuilder builder;

	for (const auto& elem: ptree) {
		if (elem.first == "x") {
			builder.set_x(std::stod(elem.second.data()));
		} else if (elem.first == "y") {
			builder.set_y(std::stod(elem.second.data()));
		} else if (elem.first == "z") {
			builder.set_z(std::stod(elem.second.data()));
		}
	}

	return builder.build();
}

std::unique_ptr<Thing> parse_thing(const boost::property_tree::ptree& ptree)
{
	int nb_points = 250000;
	const boost::property_tree::ptree::value_type& couple = *(ptree.begin());
	std::cout << couple.first << std::endl;

	if (couple.first == "cone") {
		return Cone::create(nb_points, parse_color(couple.second));
	} else if (couple.first == "cube") {
		return Cube::create(nb_points, parse_color(couple.second));
	} else if (couple.first == "cylinder") {
		return Cylinder::create(nb_points, parse_color(couple.second));
	} else if (couple.first == "sphere") {
		return Sphere::create(nb_points, parse_color(couple.second));
	} else if (couple.first == "torus") {
		return Torus::create(nb_points, parse_color(couple.second));
	} else if (couple.first == "triangle") {
		return Triangle::create();
	} else if (couple.first == "homothety") {
		auto thing = parse_thing(couple.second.get_child("child"));
		auto coord = parse_coord(couple.second);

		return Transformation::dilate(*thing, coord);
	} else if (couple.first == "rotation") {
		auto thing = parse_thing(couple.second.get_child("child"));
		auto coord = parse_coord(couple.second);

		return Transformation::rotate(*thing, coord);
	} else if (couple.first == "translation") {
		auto thing = parse_thing(couple.second.get_child("child"));
		auto coord = parse_coord(couple.second);

		return Transformation::translate(*thing, coord);
	} else if (couple.first == "intersection") {
		auto l_thing = parse_thing(couple.second.get_child("left-child"));
		auto r_thing = parse_thing(couple.second.get_child("right-child"));

		return Intersection::intersect(*l_thing, *r_thing);
	} else if (couple.first == "substraction") {
		auto l_thing = parse_thing(couple.second.get_child("left-child"));
		auto r_thing = parse_thing(couple.second.get_child("right-child"));

		return Substraction::substract(*l_thing, *r_thing);
	} else if (couple.first == "union") {
		auto l_thing = parse_thing(couple.second.get_child("left-child"));
		auto r_thing = parse_thing(couple.second.get_child("right-child"));

		return Union::unite(*l_thing, *r_thing);
	} else {
		return Hollow::create();
	}
}

std::unique_ptr<Thing> Parser::parse()
{
	boost::property_tree::ptree ptree;
	boost::property_tree::read_json(m_path, ptree);

	return parse_thing(ptree);
}
