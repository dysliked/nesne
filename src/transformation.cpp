#include "transformation.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/transform.hpp>

#include <algorithm>
#include <numbers>

Point Transformation::apply(const Point& point) const
{
	glm::vec4 vertex = m_matrix * glm::vec4(point.get_vertex(), 1.0);
	glm::vec4 normal = m_normal_matrix * glm::vec4(point.get_normal(), 1.0);

	return Point(vertex, normal, point.get_color());
}

Point Transformation::cancel(const Point& point) const
{
	glm::vec3 coord = m_inv_matrix * glm::vec4(point.get_vertex(), 1.0);

	return Point(coord, point.get_normal(), point.get_color());
}

std::unique_ptr<Thing> Transformation::dilate(Thing& thing, glm::vec3& coord) {
	glm::mat4 matrix = glm::scale(coord);
	glm::mat4 inv_matrix = glm::scale(glm::vec3(1.0 / coord[0], 1.0 / coord[1], 1.0 / coord[2]));

	return transform(thing.get_points(), std::make_unique<Transformation>(Transformation(thing.get_shape(), matrix, inv_matrix, matrix)));
}

bool Transformation::is_inside(const Point& point) const
{
	return m_shape->is_inside(cancel(point));
}

std::unique_ptr<Thing> Transformation::transform(std::vector<Point>& points, const std::shared_ptr<Transformation> shape) {
	std::vector<Point> new_points;

	std::transform(points.begin(), points.end(), std::back_inserter(new_points), [shape](const Point& point) {  return shape->apply(point); });

	return std::make_unique<Thing>(new_points, shape);
}

std::unique_ptr<Thing> Transformation::rotate(Thing& thing, glm::vec3& coord) {
	glm::mat4 x_rotate = glm::rotate(static_cast<float>(coord[0] * std::numbers::pi / 180.0), glm::vec3(1, 0, 0));
	glm::mat4 y_rotate = glm::rotate(static_cast<float>(coord[1] * std::numbers::pi / 180.0), glm::vec3(0, 1, 0));
	glm::mat4 z_rotate = glm::rotate(static_cast<float>(coord[2] * std::numbers::pi / 180.0), glm::vec3(0, 0, 1));

	glm::mat4 matrix = z_rotate * y_rotate * x_rotate;
	glm::mat4 inv_matrix = x_rotate * y_rotate * z_rotate;

	return transform(thing.get_points(), std::make_unique<Transformation>(Transformation(thing.get_shape(), matrix, inv_matrix, matrix)));
}

std::unique_ptr<Thing> Transformation::translate(Thing& thing, glm::vec3& coord) {
	glm::mat4 matrix = glm::translate(coord);
	glm::mat4 inv_matrix = glm::translate(- coord);
	//glm::mat4 normal_matrix = glm::translate(glm::vec3(0.0, 0.0, 0.0));

	return transform(thing.get_points(), std::make_unique<Transformation>(Transformation(thing.get_shape(), matrix, inv_matrix, matrix)));
}
