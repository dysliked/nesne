#include "cone.hpp"

#include "random-facade.hpp"

std::unique_ptr<Thing> Cone::create(int nb_points, const glm::vec3& color)
{
	static auto SINGLETON = std::make_shared<Cone>(Cone());
	std::unique_ptr<Point> point;
	std::vector<Point> points;

	for (int id = 0; id < nb_points; id ++)
	{
		double theta = RandomFacade::get().on_circle();
		double radius = RandomFacade::get().on_line() / 2.0;

		double xx = radius * cos(theta);
		double yy = radius * sin(theta);

		if (id < nb_points / 3)
			point = std::make_unique<Point>(glm::vec3(xx, yy, -1.0), glm::vec3(0.0, 0.0, -1.0), color);
		else
		{
			double zz = 1.0 - radius * 2.0;
			point = std::make_unique<Point>(glm::vec3(xx, yy, zz), glm::vec3(xx, yy, 1.0), color);
		}

		points.emplace_back(*point);
	}

	return std::make_unique<Thing>(points, SINGLETON);
}

bool Cone::is_inside(const Point& point) const
{
	double tmp = (1.0 - point.get_vertex()[2]) / 2.0;
	tmp *= tmp;

	return point.get_vertex()[0] * point.get_vertex()[0] + point.get_vertex()[1] * point.get_vertex()[1] < tmp && point.get_vertex()[2] > -1.0 && point.get_vertex()[2] < 1.0;
}
