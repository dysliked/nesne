#ifndef __POINT__
#define __POINT__

#include <glm/glm.hpp>

#include <ostream>

class Point
{
public:

	friend std::ostream& operator<<(std::ostream& os, const Point& point);

	explicit Point(const glm::vec3& vertex, const glm::vec3& normal, const glm::vec3& color);

	virtual ~Point() {}

	const glm::vec3& get_color() const;

	const glm::vec3& get_normal() const;

	const glm::vec3& get_vertex() const;

	glm::vec3 normal_opposite() const;

	glm::vec3 vertex_opposite() const;

private:

	glm::vec3 m_vertex;
	glm::vec3 m_normal;
	glm::vec3 m_color;
};

#endif /* __POINT__ */
