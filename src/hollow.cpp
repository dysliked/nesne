#include "hollow.hpp"

std::unique_ptr<Thing> Hollow::create() {
	static auto SINGLETON = std::make_shared<Hollow>(Hollow());
	std::vector<Point> empty;

	return std::make_unique<Thing>(empty, SINGLETON);
}

bool Hollow::is_inside(const Point& point) const {
	return false;
}
