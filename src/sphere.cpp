#include "sphere.hpp"

#include "random-facade.hpp"

#include <cmath>

std::unique_ptr<Thing> Sphere::create(int nb_points, const glm::vec3& color)
{
	static auto SINGLETON = std::make_shared<Sphere>(Sphere());
	std::vector<Point> points;

	for (int id = 0; id < nb_points; id ++)
	{
		double theta = RandomFacade::get().on_circle();
		double phi = RandomFacade::get().on_circle() / 2.0;

		double xx = cos(theta) * sin(phi);
		double yy = sin(theta) * sin(phi);
		double zz = cos(phi);

		points.emplace_back(Point(glm::vec3(xx, yy, zz), glm::vec3(xx, yy, zz), color));
	}

	return std::make_unique<Thing>(points, SINGLETON);
}

bool Sphere::is_inside(const Point& point) const
{
	return point.get_vertex()[0] * point.get_vertex()[0] + point.get_vertex()[1] * point.get_vertex()[1] + point.get_vertex()[2] * point.get_vertex()[2] < 1.0;
}
