#ifndef __UNION__
#define __UNION__

#include "thing.hpp"

class Union : public Shape {

public:

	bool is_inside(const Point& point) const override;

	static std::unique_ptr<Thing> unite(Thing& lhs, Thing& rhs);

private:

	explicit Union(std::shared_ptr<Shape> lhs, std::shared_ptr<Shape> rhs)
		: m_lhs(lhs), m_rhs(rhs)
	{}

	std::shared_ptr<Shape> m_lhs;
	std::shared_ptr<Shape> m_rhs;
};

#endif /* __UNION__ */
