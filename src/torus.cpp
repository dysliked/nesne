#include "torus.hpp"

#include "random-facade.hpp"

#include <cmath>

static const double RADIUS = 0.25;

std::unique_ptr<Thing> Torus::create(int nb_points, const glm::vec3& color)
{
	static auto SINGLETON = std::make_shared<Torus>(Torus());
	std::vector<Point> points;

	for (int id = 0; id < nb_points; id ++)
	{
		double theta = RandomFacade::get().on_circle();
		double phi = RandomFacade::get().on_circle();

		double tmp = (1 + RADIUS * cos(theta));
		double xx = tmp * cos(phi);
		double yy = tmp * sin(phi);
		double zz = RADIUS * sin(theta);

		points.emplace_back(Point(glm::vec3(xx, yy, zz), glm::vec3(xx, yy, zz), color));
	}

	return std::make_unique<Thing>(points, SINGLETON);
}

bool Torus::is_inside(const Point& point) const
{
	double radiusXY = point.get_vertex()[0] * point.get_vertex()[0] + point.get_vertex()[1] * point.get_vertex()[1];

	return (1.0 - RADIUS) * (1.0 - RADIUS) < radiusXY && radiusXY < (1.0 + RADIUS) * (1.0 + RADIUS) && -RADIUS < point.get_vertex()[2] && point.get_vertex()[2] < RADIUS;
}
