#include "cube.hpp"

#include "random-facade.hpp"

#include <cmath>

std::unique_ptr<Thing> Cube::create(int nb_points, const glm::vec3& color)
{
	static auto SINGLETON = std::make_shared<Cube>(Cube());
	std::unique_ptr<Point> point;
	std::vector<Point> points;

	for (int id = 0; id < nb_points; id ++)
	{
		double nb1 = RandomFacade::get().on_line() - 1.0;
		double nb2 = RandomFacade::get().on_line() - 1.0;
		int pair = ((id % 2 == 0)?-1:1);


		if (id < nb_points / 3)
			point = std::make_unique<Point>(glm::vec3(nb1, nb2, pair), glm::vec3(0, 0, pair), color);
		else if (id < 2 * nb_points / 3)
			point = std::make_unique<Point>(glm::vec3(nb1, pair, nb2), glm::vec3(0, pair, 0), color);
		else
			point = std::make_unique<Point>(glm::vec3(pair, nb1, nb2), glm::vec3(pair, 0, 0), color);

		points.emplace_back(*point);
	}

	return std::make_unique<Thing>(points, SINGLETON);
}

bool Cube::is_inside(const Point& point) const
{
	return std::max(std::abs(point.get_vertex()[0]), std::max(std::abs(point.get_vertex()[1]), std::abs(point.get_vertex()[2]))) <= 1.0;
}
