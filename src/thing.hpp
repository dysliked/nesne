#ifndef __THING__
#define __THING__

#include "shape.hpp"

#include <memory>
#include <vector>

class Thing {
public:

	explicit Thing(std::vector<Point>& points, std::shared_ptr<Shape> shape)
		: m_points(points), m_shape(shape)
	{}

	std::vector<Point>& get_points()
	{ return m_points; }

	std::shared_ptr<Shape> get_shape()
	{ return m_shape; }

private:

	std::vector<Point> m_points;
	std::shared_ptr<Shape> m_shape;
};

#endif /* __THING__ */
