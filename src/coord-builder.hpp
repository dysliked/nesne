#ifndef __COORD_BUILDER__
#define __COORD_BUILDER__

#include <glm/glm.hpp>

#include <istream>

class CoordBuilder
{
public:

	explicit CoordBuilder();

	virtual ~CoordBuilder() {}

	glm::vec3 build() const;

	double get_x() const;

	double get_y() const;

	double get_z() const;

	void set_x(double x);

	void set_y(double y);

	void set_z(double z);

private:

	double m_x;
	double m_y;
	double m_z;
};

std::istream& operator>>(std::istream& is, CoordBuilder& builder);

#endif // __COORD_BUILDER__
