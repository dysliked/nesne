#ifndef __TRIANGLE__
#define __TRIANGLE__

#include "thing.hpp"

class Triangle : public Shape {

public:

	static std::unique_ptr<Thing> create();

	bool is_inside(const Point& point) const override;

private:

	explicit Triangle()
	{}
};

#endif /* __TRIANGLE__ */
