#include "triangle.hpp"

std::unique_ptr<Thing> Triangle::create()
{
	static auto SINGLETON = std::make_shared<Triangle>(Triangle());
	std::vector<Point> points;

	points.emplace_back(glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	points.emplace_back(glm::vec3(1.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	points.emplace_back(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f));

	return std::make_unique<Thing>(points, SINGLETON);
}

bool Triangle::is_inside(const Point& point) const
{
	return false;
}
