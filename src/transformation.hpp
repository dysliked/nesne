#ifndef __TRANSFORMATION__
#define __TRANSFORMATION__

#include "thing.hpp"

#include <glm/glm.hpp>

static const int SIZE = 16;

class Transformation : public Shape {

public:

	Point apply(const Point& point) const;

	Point cancel(const Point& point) const;

	/**
	 * @brief Homothety
	 */
	static std::unique_ptr<Thing> dilate(Thing& thing, glm::vec3& coord);

	bool is_inside(const Point& point) const override;

	/**
	 * @brief Rotation
	 */
	static std::unique_ptr<Thing> rotate(Thing& thing, glm::vec3& coord);

	/**
	 * @brief Translation
	 */
	static std::unique_ptr<Thing> translate(Thing& thing, glm::vec3& coord);

private:

	explicit Transformation(const std::shared_ptr<Shape> shape, const glm::mat4& matrix, const glm::mat4& inv_matrix, const glm::mat4& normal_matrix)
		: m_shape(shape), m_matrix(matrix), m_inv_matrix(inv_matrix), m_normal_matrix(normal_matrix)
	{}

	static std::unique_ptr<Thing> transform(std::vector<Point>& points, const std::shared_ptr<Transformation> trans);

	std::shared_ptr<Shape> m_shape;
	glm::mat4 m_matrix;
	glm::mat4 m_inv_matrix;
	glm::mat4 m_normal_matrix;
};

#endif // __TRANSFORMATION__
