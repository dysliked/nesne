#include "substraction.hpp"

#include <algorithm>

bool Substraction::is_inside(const Point& point) const
{
	return m_lhs->is_inside(point) && !m_rhs->is_inside(point);
}

std::unique_ptr<Thing> Substraction::substract(Thing& lhs, Thing& rhs) {
	std::vector<Point> points;
	std::vector<Point> lhs_points = lhs.get_points();
	std::vector<Point> rhs_points = rhs.get_points();

	const auto r_shape = rhs.get_shape();

	std::copy_if(lhs_points.begin(), lhs_points.end(), std::back_inserter(points), [r_shape](const Point& point){ return !r_shape->is_inside(point); });

	for (const Point& point: rhs_points) {
		if (lhs.get_shape()->is_inside(point)) {
			points.push_back(Point(point.get_vertex(), point.normal_opposite(), point.get_color()));
		}
	}

	auto substraction = std::make_shared<Substraction>(Substraction(lhs.get_shape(), rhs.get_shape()));

	return std::make_unique<Thing>(points, substraction);
}
