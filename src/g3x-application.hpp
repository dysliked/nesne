#ifndef __G3X_APPLICATION__
#define __G3X_APPLICATION__

#include "thing.hpp"

#include <memory>

class G3XApplication {
public:

	static G3XApplication& get();

	int run(Thing* thing);

private:

	explicit G3XApplication();

	std::string m_progname;
};

#endif /* __G3X_APPLICATION__ */
