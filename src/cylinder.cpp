#include "cylinder.hpp"

#include "random-facade.hpp"

#include <cmath>

std::unique_ptr<Thing> Cylinder::create(int nb_points, const glm::vec3& color)
{
	static auto SINGLETON = std::make_shared<Cylinder>(Cylinder());
	std::unique_ptr<Point> pt;
	std::vector<Point> points;

	for (int id = 0; id < nb_points; id ++)
	{
		double theta = RandomFacade::get().on_circle();
		double pos = RandomFacade::get().on_line() - 1.0;

		double xx = cos(theta);
		double yy = sin(theta);

		if (id < nb_points / 2)
			pt = std::make_unique<Point>(glm::vec3(xx, yy, pos), glm::vec3(xx, yy, 0.0), color);
		else
		{
			double radius = RandomFacade::get().on_line() / 2.0;
			double zz = (pos > 0) ? -1.0 : 1.0;

			xx *= radius;
			yy *= radius;

			pt = std::make_unique<Point>(glm::vec3(xx, yy, zz), glm::vec3(0.0, 0.0, zz), color);
		}

		points.emplace_back(*pt);
	}

	return std::make_unique<Thing>(points, SINGLETON);
}

bool Cylinder::is_inside(const Point& point) const
{
	return point.get_vertex()[0] * point.get_vertex()[0] + point.get_vertex()[1] * point.get_vertex()[1] <= 1.0 && point.get_vertex()[2] >= -1.0 && point.get_vertex()[2] <= 1.0;
}
