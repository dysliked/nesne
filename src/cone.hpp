#ifndef __CONE__
#define __CONE__

#include "thing.hpp"

class Cone : public Shape {

public:

	static std::unique_ptr<Thing> create(int nb_points, const glm::vec3& color);

	bool is_inside(const Point& point) const override;

private:

	explicit Cone()
	{}
};

#endif /* __CONE__ */
