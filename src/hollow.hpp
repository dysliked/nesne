#ifndef __HOLLOW__
#define __HOLLOW__

#include "thing.hpp"

class Hollow : public Shape {

public:

	static std::unique_ptr<Thing> create();

	bool is_inside(const Point& point) const override;

private:

	explicit Hollow()
	{}
};

#endif /* __HOLLOW__ */