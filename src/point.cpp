#include "point.hpp"

Point::Point(const glm::vec3& vertex, const glm::vec3& normal, const glm::vec3& color)
	: m_vertex(vertex), m_normal(normal), m_color(color)
{
}

const glm::vec3& Point::get_normal() const
{
	return m_normal;
}

const glm::vec3& Point::get_vertex() const
{
	return m_vertex;
}

const glm::vec3& Point::get_color() const
{
	return m_color;
}

glm::vec3 Point::normal_opposite() const
{
	return glm::vec3(-m_normal[0], -m_normal[1], -m_normal[2]);
}

glm::vec3 Point::vertex_opposite() const
{
	return glm::vec3(-m_vertex[0], -m_vertex[1], -m_vertex[2]);
}

std::ostream& operator<<(std::ostream& os, const Point& point)
{
	os << "Point(" << point.m_vertex[0] << "," << point.m_vertex[1] << "," << point.m_vertex[2] << ")";
	return os;
}
