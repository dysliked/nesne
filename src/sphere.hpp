#ifndef __SPHERE__
#define __SPHERE__

#include "thing.hpp"

class Sphere : public Shape {

public:

	static std::unique_ptr<Thing> create(int nb_points, const glm::vec3& color);

	bool is_inside(const Point& point) const override;

private:

	explicit Sphere()
	{}
};

#endif /* __SPHERE__ */
