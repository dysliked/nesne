#ifndef __CUBE__
#define __CUBE__

#include "thing.hpp"

class Cube : public Shape {

public:

	static std::unique_ptr<Thing> create(int nb_points, const glm::vec3& color);

	bool is_inside(const Point& point) const override;

private:

	explicit Cube()
	{}
};

#endif /* __CUBE__ */
