#ifndef __PARSER__
#define __PARSER__

#include "thing.hpp"

#include <memory>
#include <string>

class Parser
{
public:

	explicit Parser(const std::string& path)
		: m_path(path)
	{}

	std::unique_ptr<Thing> parse();

private:

	std::string m_path;
};

#endif /* __PARSER__ */
