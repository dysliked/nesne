#ifndef __CYLINDER__
#define __CYLINDER__

#include "thing.hpp"

class Cylinder : public Shape {

public:

	static std::unique_ptr<Thing> create(int nb_points, const glm::vec3& color);

	bool is_inside(const Point& point) const override;

private:

	explicit Cylinder()
	{}
};

#endif /* __CYLINDER__ */
