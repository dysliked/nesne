#include "random-facade.hpp"

#include <memory>
#include <numbers>

RandomFacade::RandomFacade()
	: m_circle_dist(0.0, 2 * std::numbers::pi), m_line_dist(0.0, 2.0)
{
}

RandomFacade& RandomFacade::get()
{
	static std::unique_ptr<RandomFacade> singleton = std::make_unique<RandomFacade>();

	return *singleton;
}

double RandomFacade::on_circle()
{
	return m_circle_dist(m_generator);
}

double RandomFacade::on_line()
{
	return m_line_dist(m_generator);
}
