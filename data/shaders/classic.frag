#version 330 core

precision highp float;

in vec3 out_VertColor;

out vec4 out_FragColor;

void main() {
	out_FragColor = vec4(out_VertColor, 1.0);
}
