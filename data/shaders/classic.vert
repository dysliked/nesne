#version 330 core

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Color;

out vec3 out_VertColor;

void main() {
	gl_Position.xyz = in_Position;
	gl_Position.w = 1.0;

	out_VertColor = in_Color;
}
